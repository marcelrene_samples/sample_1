-- Tabla stage */
DROP TABLE IF EXISTS elementos_red.ext_reportes_enre;
CREATE EXTERNAL TABLE elementos_red.ext_reportes_enre(
fecha           STRING
,hora            STRING
,tipo            STRING
,empresa         STRING
,latitud         STRING
,longitud        STRING
,partido         STRING
,localidad       STRING
,subestacion     STRING
,alimentador     STRING
,usuarios        STRING
,normalizacion   STRING
)row format delimited fields terminated by '|'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/reportes_enre/Stage/";

-- Tabla con datos crudos desde GVP/Argentina/IPTV/ChannelMaps/
DROP TABLE IF EXISTS elementos_red.reportes_enre;
CREATE TABLE IF NOT EXISTS elementos_red.reportes_enre(
fecha           date
,hora            STRING
,tipo            STRING
,empresa         STRING
,latitud         STRING
,longitud        STRING
,partido         STRING
,localidad       STRING
,subestacion     STRING
,alimentador     STRING
,usuarios        STRING
,normalizacion   STRING
,ORIGIN_FILE_NAME            STRING
,fecha_ingesta               timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;