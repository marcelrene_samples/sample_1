DROP TABLE IF EXISTS elementos_red.reportes_enre_tmp;
CREATE TABLE IF NOT EXISTS elementos_red.reportes_enre_tmp(
fecha           date
,hora            STRING
,tipo            STRING
,empresa         STRING
,latitud         STRING
,longitud        STRING
,partido         STRING
,localidad       STRING
,subestacion     STRING
,alimentador     STRING
,usuarios        STRING
,normalizacion   STRING
,ORIGIN_FILE_NAME            STRING
,fecha_ingesta               timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;

INSERT INTO elementos_red.reportes_enre_tmp PARTITION (day)  
SELECT 
cast(fecha as date) as fecha
,hora
,tipo
,empresa
,latitud
,longitud
,partido
,localidad
,subestacion
,alimentador
,usuarios
,normalizacion
,REVERSE(SPLIT(REVERSE(INPUT__FILE__NAME),'/')[0]) as ORIGIN_FILE_NAME
,cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta
,translate(fecha,"-", "") as day
FROM elementos_red.ext_reportes_enre; 