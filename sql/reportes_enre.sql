INSERT INTO elementos_red.reportes_enre PARTITION (day)  
SELECT 
a.fecha
,a.hora
,a.tipo
,a.empresa
,a.latitud
,a.longitud
,a.partido
,a.localidad
,a.subestacion
,a.alimentador
,a.usuarios
,a.normalizacion
,a.ORIGIN_FILE_NAME
,a.fecha_ingesta
,a.day
FROM elementos_red.reportes_enre_tmp a
LEFT JOIN 
(SELECT * FROM elementos_red.reportes_enre WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.fecha as string),'') = nvl(cast(b.fecha as string),'') and
  nvl(a.hora ,'') = nvl(b.hora,'') and
  nvl(a.tipo,'') = nvl(b.tipo,'') and
  nvl(a.empresa,'') = nvl(b.empresa,'') and
  nvl(a.latitud,'') = nvl(b.latitud,'') and
  nvl(a.longitud,'') = nvl(b.longitud,'') AND
  nvl(a.partido,'') = nvl(b.partido,'') and
  nvl(a.localidad,'') = nvl(b.localidad,'') AND
  nvl(a.subestacion,'') = nvl(b.subestacion,'') AND
  nvl(a.alimentador,'') = nvl(b.alimentador,'') AND
  nvl(a.usuarios,'') = nvl(b.usuarios,'') and
  nvl(a.normalizacion,'') = nvl(b.normalizacion,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')
WHERE
  a.day = ${FECHA_A_PROCESAR} and
b.fecha is null and
b.hora is null and
b.tipo is null and
b.empresa is null and
b.latitud is null and
b.longitud is null and
b.partido is null and
b.localidad is null and
b.subestacion is null and
b.alimentador is null and
b.usuarios is null and
b.normalizacion is null and
b.ORIGIN_FILE_NAME is null and
b.day is null;