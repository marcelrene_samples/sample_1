#!/usr/bin/env bash
# Este script se encarga de ejecutar con beeline un archivo .sql
# pasado como segundo parámetro $2
# El parámetro $1 es el archivo de configuración

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils.sh"
source "${bin_path}""../conf/$1" #Archvio de configuración con los parámetros de conexión

interfaz=$process_name
SQL_FILE=$2

AVAILABLE_MODES=$modo_conf
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - " "Archivo de configuración no válido, incluya como primer parámetro el archivo correcto" | tee -a $log_file
        exit 1
fi

if [[ !($(echo $SQL_FILE | grep ".sql$"))  ]]; then
  echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - ""Error en la ejecución, Parámetro 2 debe ser un archivo válido .sql" | tee -a $log_file
  exit 1
fi

exec > $log_file 2>&1
log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0")  "

HIVE_CONNECTION_STRING="$path_beeline"
HIVEVAR_TEZ_QUEUE="$queue"
HIVE_VARS_STRING="$(hive_vars_to_string HIVEVAR_)"
SCRIPT_SQL_FILE="${sql_local_path}${SQL_FILE}"
HIVE_ADDITIONAL_OPTIONS=""
HQL_CMD="execute_hql_script \"$HIVE_CONNECTION_STRING\" \"$HIVE_VARS_STRING\" \"$SCRIPT_SQL_FILE\" \"$HIVE_ADDITIONAL_OPTIONS\""

execute "$HQL_CMD" "EJECUCION DE $SCRIPT_SQL_FILE OK" "EJECUCION DE $SCRIPT_SQL_FILE FALLO"

log 0 "$log_file" "'\033[0;32m'EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE '\033[0m' "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0
