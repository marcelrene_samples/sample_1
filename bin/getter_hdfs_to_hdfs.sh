#!/usr/bin/env bash

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils.sh"
source "${bin_path}""../conf/$1" #Archvio de configuración con los parámetros de conexión

AVAILABLE_MODES=$modo_conf
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - " "Archivo de configuración no válido, incluya como primer parámetro el archivo correcto" | tee -a $log_file
        exit 1
fi

#Log que captura toda la salida del proceso
exec > $log_file 2>&1

log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0") "

if  [ "$2" == "-a" ]; then
  execute "hdfs dfs -ls $source_path  | grep .txt | tail -n 1 > ${server_files}" "Obtencion del último archivo en el directorio ${source_path}." "Error en la obtencion último archivo en el directorio ${source_path}"
  execute "cat ${server_files} | rev | cut -d' ' -f 1 | rev |  rev | cut -d'/' -f 1 | rev | cut -d'.' -f 1 > ${tmp_local_path}\"last_file_date\""  "Éxito en la obtención de la última fecha del archivo cargado" "Fallo en la obtención de la última fecha del archivo cargado"
  filename="" 
  execute "hdfs dfs -ls ${source_path}${filename} | grep .txt | rev | cut -d'/' -f 1 | rev > ${server_files}" "Obtencion de archivos en el directorio de ${source_path}$ exitosa." "Error en la obtencion de archivos en el directorio de: ${source_path}"

  log 0 "$log_file" "-----------------------------------------------------------"
  log 0 "$log_file" "LISTADO DE ARCHIVOS A DESCARGAR HOY $(date "+%Y-%m-%d-%H:%M:%S")"
  log 0 "$log_file" "  "
  cat ${server_files} | tee -a $log_file
  log 0 "$log_file" "----------------------------------------------------------- "
  #Verifica que haya nuevos archivos para descargar, si no hay nuevos archivos genera error y termina el proceso
  if [ $(cat ${server_files} | wc -l) -eq 0 ]; then
    log 2 "$log_file"  " Error en la ejecución, no hay archivos nuevos para descargar "
    enviar_mail "No hay archivos nuevos para descargar, verificar con la fuente" "$entorno - ERROR - $(readlink -f "$0")"  
    exit 1
  else
    while read  line;
          do
              file=$(echo $line | rev | cut -d'/' -f 1 | rev)
              #Copia archivo del directorio HDFS a linux y lo remueve de HDFS  
              if [ $(hdfs dfs -ls "${destination_path}${file}" | wc -l) -eq 1 ]; then
                execute "hdfs dfs -rm ${destination_path}${file}" "Archivo preexistente removido: ${destination_path}${file}" "Fallo al remover archivo preexistente ${destination_path}${file}"
              fi
              execute "hdfs dfs -mv ${source_path}${file} ${destination_path}" "Archivo ${source_path}${file} descargado en ${destination_path}" "Error al descargar el archivo ${source_path}${file} en ${destination_path}"
              
          done <  ${server_files}
  fi

else
  
  execute "hdfs dfs -ls ${source_path}${filename} | grep .txt | rev | cut -d'/' -f 1 | rev | sort > ${server_files}" "Obtencion de archivos en el directorio de ${source_path} exitosa." "Error en la obtencion de archivos en el directorio de: ${source_path}"

  log 0 "$log_file" "-----------------------------------------------------------"
  log 0 "$log_file" "LISTADO DE ARCHIVOS A DESCARGAR HOY $(date "+%Y-%m-%d-%H:%M:%S")"
  log 0 "$log_file" "  "
  cat ${server_files} | tee -a $log_file
  log 0 "$log_file" "----------------------------------------------------------- "
  #Verifica que haya nuevos archivos para descargar, si no hay nuevos archivos genera error y termina el proceso
  if [ $(cat ${server_files} | wc -l) -eq 0 ]; then
    log 2 "$log_file"  " Error en la ejecución, no hay archivos nuevos para descargar "
    enviar_mail "No hay archivos nuevos para descargar, verificar con la fuente" "$entorno - ERROR - $(readlink -f "$0")"  
    exit 1
  else
    filename="$(cat ${tmp_local_path}last_file_date)"
    while read  line;
          do
              file=$(echo $line | rev | cut -d'/' -f 1 | rev)
              if [ $(echo $file | rev | cut -d'.' -f 2 | rev) -gt $filename ]; then
                log 0 ""$log_file"" "descargando el archivo: $file"  
                if [ $(hdfs dfs -ls "${destination_path}${file}" | wc -l) -eq 1 ]; then
                  execute "hdfs dfs -rm ${destination_path}${file}" "Archivo preexistente removido: ${destination_path}${file}" "Fallo al remover archivo preexistente ${destination_path}${file}"
                fi
                  execute "hdfs dfs -mv ${source_path}${file} ${destination_path}" "Archivo ${source_path}${file} descargado en ${destination_path}" "Error al descargar el archivo ${source_path}${file} en ${destination_path}"
                  execute "echo $file | rev | cut -d'.' -f 2 | rev > ${tmp_local_path}\"last_file_date\"" "Éxito en la obtención de la última fecha del archivo cargado" "Fallo en la obtención de la última fecha del archivo cargado" 
              else
                  execute "hdfs dfs -rm ${source_path}${file}" "Archivo removido de ${source_path} porque ya fue cargado" "Fallo al remover archivo de ${source_path}"
              fi
          done <  ${server_files}
  fi
fi

log 0 "$log_file" "----------------------------------------------------------------- "
log 0 "$log_file" "EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0