#!/usr/bin/env bash
# Este script realiza las llamadas a beeline para cargar los archivos 
# en hdfs, primero los carga en una tabla particionada por fecha de proceso y luego los carga en la 
# tabla tabla final particionada por fecha de evento.

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"


bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils.sh"
source "${bin_path}""../conf/$1" #Archvio de configuración con los parámetros de conexión

interfaz=$process_name


AVAILABLE_MODES=$modo_conf
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - " "Archivo de configuración no válido, incluya como primer parámetro el archivo correcto" | tee -a $log_file
        exit 1
fi

exec > $log_file 2>&1
log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0") "

#Si algún archivo quedó en Stage/ se devuelve a A_Procesar/
if [ $(hdfs dfs -ls -C "${stagepath}${stage_folder}" | wc -l) -gt 0 ]; then 
	(hdfs dfs -ls -C "${stagepath}${stage_folder}") | while read line;
	do
    file=$(echo $line | rev | cut -d'/' -f 1 | rev)
    if [ $(hdfs dfs -ls "${stagepath}${a_procesar_folder}${file}" | wc -l) -eq 1 ]; then
		  execute "hdfs dfs -rm ${stagepath}${a_procesar_folder}${file}" "Archivo preexistente $line removido de ${stagepath}${a_procesar_folder}" "Fallo en la remoción de $line preexistente desde ${stagepath}${a_procesar_folder}"
      execute "hdfs dfs -mv $line ${stagepath}${a_procesar_folder}" "Move exitoso de $line a ${stagepath}${a_procesar_folder}" "Error en move de $line a ${stagepath}${a_procesar_folder}"
    else
      execute "hdfs dfs -mv $line ${stagepath}${a_procesar_folder}" "Move exitoso de $line a ${stagepath}${a_procesar_folder}" "Error en move de $line a ${stagepath}${a_procesar_folder}"
    fi	
  done
fi
#Se verifica que haya archivos para procesar
if [ $(hdfs dfs -ls -C "${stagepath}${a_procesar_folder}" | wc -l) -eq 0 ]; then
	log 2 "${log_file}" "Error, no hay nada para insertar en ${process_name}"
  exit 1
else
	execute "hdfs dfs -mv ${stagepath}${a_procesar_folder}* ${stagepath}${stage_folder}" "Transferencia exitosa de archivos a:${stagepath}${stage_folder}" "Error al mover los archivos a:${stagepath}${stage_folder}"
fi
	
curr_date="$(date "+%Y%m%d")"
FECHA_PROCESO="'$curr_date'"
HIVE_CONNECTION_STRING="$path_beeline"
HIVEVAR_TEZ_QUEUE="$queue"
HIVEVAR_CURR_DATE="$FECHA_PROCESO"
HIVE_VARS_STRING="$(hive_vars_to_string HIVEVAR_)"
SCRIPT_SQL_FILE="${sql_local_path}reportes_enre_tmp.sql"
HIVE_ADDITIONAL_OPTIONS=""
HQL_CMD="execute_hql_script \"$HIVE_CONNECTION_STRING\" \"$HIVE_VARS_STRING\" \"$SCRIPT_SQL_FILE\" \"$HIVE_ADDITIONAL_OPTIONS\""

log 0 "$log_file" "Iniciando carga de tabla reportes_enre_tmp.sql"
execute "$HQL_CMD" "OK al insertar en la tabla reportes_enre_tmp.sql" "FALLO en la carga de la tabla reportes_enre_tmp.sql"

# Se buscan las diferentes fechas de eventos para realizar las particiones a partir de estas
log 0 "$log_file" "$HIVE_VARS_STRING, $HIVEVAR_CURR_DATE"
execute "beeline --silent==true -u \"$path_beeline\" --hivevar TEZ_QUEUE=$queue --hivevar CURR_DATE="$FECHA_PROCESO" --outputformat=dsv --showHeader=false --delimiterForDSV=\| -f \"${sql_local_path}distinct_dates_reportes_enre.sql\" > ${tmp_local_path}dates" "Se cargo correctamente el archivo date con las fechas de  que tiene la particion de la tabla elementos_red.reportes_enre_tmp" "Error al crear archivo dates con todas las particiones de la tabla elementos_red.reportes_enre_tmp"

#SET_HIVE_VARS=$SET_HIVE_VARS" --hivevar FECHA_A_PROCESAR="
HIVEVAR_FECHA_A_PROCESAR=
HIVE_VARS_STRING="$(hive_vars_to_string HIVEVAR_)"


log 0 "$log_file" "Iniciando iteración por fechas de evento. Se inserta cada una como particion en la tabla final elementos_red.reportes_enre"
	
	# Iteración por todas las fechas de evento para generar una tabla final para cada una de ellas con las tablas temporales particionadas por fecha de proceso    
    for date in $(cat "${tmp_local_path}dates"); do
      log 0 "$log_file" "Iniciando carga de ${sql_local_path}reportes_enre.sql LA FECHA ES ${date}"
      HIVE_VARS_FECHA=$(echo $HIVE_VARS_STRING | sed -e "s/FECHA_A_PROCESAR=/FECHA_A_PROCESAR='${date}'/g")
      #
      log 0 "$log_file" "Las variables son: $HIVE_VARS_STRING"
      #
      SCRIPT_SQL_FILE="${sql_local_path}reportes_enre.sql"
      HQL_CMD="execute_hql_script \"$HIVE_CONNECTION_STRING\" \"$HIVE_VARS_FECHA\" \"$SCRIPT_SQL_FILE\" \"$HIVE_ADDITIONAL_OPTIONS\""
      execute "$HQL_CMD" "Carga exitosa de ${sql_local_path}reportes_enre.sql con fecha de evento $date" "Error en la carga de ${sql_local_path}reportes_enre.sql con fecha de evento $date"
    done 
			
	execute "hdfs dfs -rm -skipTrash ${stagepath}${stage_folder}* " "Se eliminaron los archivos de manera exitosa de la carpeta ${stagepath}${stage_folder}" "Error al eliminar los archivos de la carpeta ${stagepath}${stage_folder}"

log 0 "$log_file" "'\033[0;32m'EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE  '\033[0m' "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0